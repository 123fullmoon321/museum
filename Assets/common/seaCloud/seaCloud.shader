﻿Shader "common/seaCloud"
{
	Properties
	{
		_noiseSource ("_noiseSource", 2D) = "" {}
		_cloudY ("_cloudY", Float) = 0
		_topColor ("_topColor", Color) = (0.8, 0.9, 0.9, 1.0)
		_bottomColor ("_bottomColor", Color) = (0.3, 0.4, 0.5, 0.8)
		_fogColor ("_fogColor", Color) = (0.8, 0.9, 1.0, 1.0)
		_fogForce ("_fogForce", Range (0, 1)) = 0.3
	}
	SubShader
	{
        Tags 
		{
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
		Cull Front

		Pass
		{
			Blend SrcAlpha OneMinusSrcAlpha
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"
			#include "UnityShaderVariables.cginc"
			
			uniform sampler2D _noiseSource;
			uniform float4 _topColor;
			uniform float4 _bottomColor;
			uniform float _cloudY;
			uniform float4 _fogColor;
			uniform float _fogForce;

			// ====vertex====
			struct v2f
			{
				float4 vertex : SV_POSITION;
				float4 worldPosition : TEXCOORD0;
			};

			v2f vert (float4 vertex : POSITION)
			{
				v2f o;
				// 必要なワールド位置を引き継ぐ
				float4 worldPosition = mul(unity_ObjectToWorld, vertex);
				o.worldPosition = worldPosition;
				o.vertex = UnityObjectToClipPos(vertex);
				return o;
			}

			// ====fragment====
            struct frag_out
            {
                fixed4 color : SV_TARGET;
                float depth : SV_DEPTH;
            };

			/*
			中央が0のノイズを返す
			*/
			float createNoise(float2 xz, float noiseSwing, float scale, float2 speed)
			{
				float2 uv = xz / scale;
				uv -=  speed * _Time.x;
				float noise = (tex2D(_noiseSource, uv).r - 0.5) * noiseSwing;
				return noise;
			}

			/*
			複数ノイズを組み合わせて雲を作る
			*/
			float createCloudNoise(float2 xz)
			{
				float noise = 0.5;
				float speed = float2(1, 1);
				float swing = 0.5;
				noise += createNoise(xz, swing, 160, speed * -0.2);
				swing *= 0.5;
				noise += createNoise(xz, swing, 60, speed * -0.05);
				swing *= 0.5;
				noise += createNoise(xz, swing, 20, speed * 0.5);
				swing *= 0.5;
				noise += createNoise(xz, swing, 4, speed * 2);
				return noise;
			}

			frag_out frag (v2f i)
			{
				// 値の準備
				frag_out o;
				// カメラへの視点ベクトル
				float3 viewDir = normalize(UnityWorldSpaceViewDir(i.worldPosition));
				// カメラからの視点ベクトル
				float3 cameraDir = -viewDir;
				// カメラから雲高さまでの倍率を得る
				float cameraToCloudRate = (_cloudY - _WorldSpaceCameraPos.y) / cameraDir.y;
				// 倍率を元に座標計算
				float3 cameraToCloud = cameraDir * cameraToCloudRate;
				float3 cloudPos = _WorldSpaceCameraPos + cameraToCloud;
				// y座標はトートロジーを起こすので直接代入
				cloudPos.y = _cloudY;
				// ノイズを生成
				float noise = createCloudNoise(cloudPos.xz);
				float4 cloudColor = noise * _topColor + (1 - noise) * _bottomColor;

				// 近距離度合いを算出（直上直下が1、無限遠が0）
				float verticality = abs(atan2(cameraToCloud.y, length(cameraToCloud.xz)));
				// フォグの強さ(遠方からforceの量までで0になる)
				float fogRate = 1 - clamp(verticality / _fogForce, 0, 1);
				cloudColor = fogRate * _fogColor + (1 - fogRate) * cloudColor;
				// カメラ倍率がマイナス（カメラの後方に雲がある）の場合ピクセル描画を破棄
				clip(cameraToCloudRate);
				// デバッグ
				// float3 debug3 = frac(cloudPos);
				// debug3.g = cameraToCloudRate;
				// o.color = fixed4(debug3, 1);
				// o.color = fixed4(debug3.x, debug, debug3.z, 1);
				// float debug = fogRate;
				// o.color = fixed4(fogRate, 0.5, 0.5, 1);
				// 値の出力
				o.color = cloudColor;
				o.depth = 0;
				return o;
			}
			ENDCG
		}
	}
}
